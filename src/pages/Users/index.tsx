import axios from "axios";
import {IProp, IState} from "./type";
import { useEffect, useState } from "react";

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';



const pages = ['Products', 'Pricing', 'Blog'];


function Users(props: IProp) {
    const [userList, setUserList] = useState <IState["userList"]>([]);
    const [newName, setNewName ] = useState ("");
    const [newPassword, setNewPassword] = useState ("");
    const [newEmail, setNewEmail] = useState ("");
    const [newRole, setNewRole ] = useState ("");


    useEffect(() => {
        handleRequestUsers();
    }, []);


const handleRequestUsers = async () => {
    try{
        const response = await axios.get("http://localhost:5173/api/user");
        setUserList(response.data);
    }
    catch (error){
        alert('Error')
    }
    
};

const handleAddUser = async () => {
    console.log(Users);
    try {
        const newUser = {
            name: newName,
            password: newPassword,
            email: newEmail,
            role: newRole
        };
        const response = await axios.post("http://localhost:5173/api/user", newUser);
        setUserList([...userList, response.data]);
        
        setNewName("");
        setNewPassword("");
        setNewEmail("");
        setNewRole("");
    } catch (error) {
        alert('Error adding user');
    } 
};
//////



return(
  
    <div>
             <AppBar position="static">
                  <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1, textAlign: 'left' }}>
                      Butchokoy App
                    </Typography>
                    <Button color="inherit">Login</Button>
                    <Button color="inherit">Users</Button>
                    <Button color="inherit">Post</Button>
                  </Toolbar>
      </AppBar>
      <br/>
        <div>
            <form>
            
            <TextField required id="outlined-required" label="Name" value = {newName} onChange={(e) => setNewName(e.target.value)}/><br/><br/>
            <TextField type = "password" required id="outlined-required" label="Password" value = {newPassword} onChange={(e) => setNewPassword(e.target.value)}/><br/><br/>
            <TextField required id="outlined-required" label="Email" value = {newEmail} onChange={(e) => setNewEmail(e.target.value)}/><br/><br/>
            <select value={newRole} onChange={(e) => setNewRole(e.target.value)}>
                    <option>Admin</option>
                    <option>Lead</option>
                    <option>Dev</option>    
            </select><br/><br/>
                
                <Button variant="contained" href="#contained-buttons" onClick={handleAddUser}>Add</Button><br/>
            </form><br/>
        </div>

        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
            <TableHead>
                <TableRow>
                    <TableCell align="right">ID</TableCell>
                    <TableCell align="right">Name</TableCell>
                    <TableCell align="right">Email</TableCell>
                    <TableCell align="right">Password</TableCell>
                    <TableCell align="right">Role</TableCell>
                    
                </TableRow>
            </TableHead>    
            <TableBody>
          {userList.map((item) => (
            <TableRow
              key={item._id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell align="right">{item._id}</TableCell>
              <TableCell align="right">{item.name}</TableCell>
              <TableCell align="right">{item.email}</TableCell>
              <TableCell align="right">{item.password}</TableCell>
              <TableCell align="right">{item.role}</TableCell>
              
            </TableRow>
          ))}
        </TableBody>
      </Table>
            
    </TableContainer>
    </div>
)
}

export default Users