export interface IProp{}

export interface IState {
    temp_input: string;
    temp_message: string;
    input_list: ITodo[];
}

export interface ITodoDisplayProp{
    inputlist: ITodo[];
    showTable: boolean;
}

interface ITodo {
    input: string;
    message: string;
    
}



