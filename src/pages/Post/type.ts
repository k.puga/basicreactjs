export interface IProp {}

export interface IState {
    postList: Post[]
}

interface Post {
    _id: string;
    title: string;
    message: string;
    status: string;
}