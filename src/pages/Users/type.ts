export interface IProp {}

export interface IState {
    userList: User[]
}

interface User {
    _id: string;
    name: string;
    username: string;
    email: string;
    password: string;
    role: string;
}