import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

import{
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";


import Home from "./pages/Home";
import Login  from "./pages/Login";
import Register from "./pages/Register";
import Users from "./pages/Users";
import Post from "./pages/Post";

import {protectedRoute} from "./utils/portectedRoute";


const router = createBrowserRouter([
    {
        path: "/home",
        element: <Home />,
      //  loader: protectedRoute
    },

    {
        path: "/login",
        element: <Login/>
      //  loader: isLoggedIn
    }, 

    {
        path: "/register",
        element: <Register/>,
        loader: protectedRoute
    },

    {
        path: "/users",
        element: <Users/>,
    },

    {
        path: "/post",
        element: <Post/>,
    }, 

    {
        path: "*",
        element: <h1>Error 404: Botchukoy not found</h1>
    },

])

function App() {
 return(
   <RouterProvider router={router} />
);
}

export default App
