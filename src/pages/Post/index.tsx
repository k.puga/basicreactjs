import axios from "axios";
import { IState } from "./type";
import  { useEffect, useState } from "react";

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import TextField from '@mui/material/TextField';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';



function Post() { 
    const [postList, setPostList] = useState<IState["postList"]>([]);
    const [newTitle, setNewTitle] = useState ("");
    const [newMessage, setNewMessage] = useState ("");
    const [newStatus, setNewStatus] = useState ("");
    
    useEffect(() => {
        handlePostList();
    }, []);

    const handlePostList = async () => {
        try {
            const response = await axios.get(
                "http://localhost:5173/api/post"
            );
            setPostList(response.data);
        } catch (error) {
            alert('Error')
        }
    };

    const handleAddPost = async () => {
        try{
       
            const newPost = {
                title: newTitle,
                message: newMessage,
                status: newStatus,
            };
            const response = await axios.post("http://localhost:5173/api/post",newPost);
            setPostList([...postList,response.data]);

            setNewTitle("");
            setNewMessage("");
            setNewStatus("");
        }
        catch(error) {
            alert("Error adding Post")
        }
    }
/////// End of ADD function

const handleDeletePost = async (_id:string) => {
        try {
          const response = await axios.delete(`http://localhost:5173/api/post/${_id}`);
          setPostList(prevState => prevState.map(
            item => item._id === _id ? {...item, active: false}: item));
            alert("Marked as INACTIVE")
        }

        catch(error){
            alert("Error deleting Post")
        }
}

////// End of SoftDelete function
    return (
<div>
      <AppBar position="static">
                  <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1, textAlign: 'left' }}>
                      Butchokoy App
                    </Typography>
                    <Button color="inherit">Login</Button>
                    <Button color="inherit">Users</Button>
                    <Button color="inherit">Post</Button>
                  </Toolbar>
      </AppBar>
      <br/>
            <div>
            <FormControl sx={{ m: 0, minWidth: 120 }}>

                    
                    <TextField type="text" placeholder="Title" value={newTitle} onChange={(e)=> setNewTitle(e.target.value)} required/><br />
                    <TextField placeholder="Enter Message" value={newMessage} onChange={(e) => setNewMessage(e.target.value)} required/><br />
                    <label>Status</label><br/>
                    <Select labelId="demo-select-small-label" id="demo-select-small" value={newStatus} onChange={(e) => setNewStatus(e.target.value)}>
                        <MenuItem>ACTIVE</MenuItem>
                        <MenuItem>INACTIVE</MenuItem>
                    </Select>    <br/><br/>
                    <Button variant="contained" onClick={handleAddPost} type = "submit">Add</Button><br />
            </FormControl><br />
            </div>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell align="right">Title</TableCell>
            <TableCell align="right">Message</TableCell>
            <TableCell align="right">Status</TableCell>
            <TableCell align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {postList.map((item) => (
            <TableRow
              key={item._id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
               
                <TableCell align="right">{item.title}</TableCell>
                <TableCell align="right">{item.message}</TableCell>
                <TableCell align="right">{item.status}</TableCell>
                <TableCell align="right">
                  <Button color="error" variant="contained" onClick={() => handleDeletePost(item._id)}>Delete</Button>&nbsp;
                  <Button  variant="contained">Edit</Button>
                </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
        </div>
    )
}

export default Post;
