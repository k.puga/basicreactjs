import React, { useState } from "react";
import { ITodo, IProp, IState } from "./type";
import "./style.css";

import TodoDisplay from "./components/TodoDisplay";
import TodoForm from "./components/TodoForm";

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Button } from "@mui/material";



const Home = (props: IProp) => {
 // const navigate = ();
  const [state, setState] = useState<IState>({
    temp_input: "",
    temp_message: "",
    input_list: [],
    showTable: false,
    editItem: {
      id: "",
      input: "",
      message: "",
    },
  });

  

  const inputHandler = (event: any) => {
    const { value } = event.target;
    setState({ ...state, temp_input: value });
  };

  const messageHandler = (event: any) => {
    const { value } = event.target;
    setState({ ...state, temp_message: value });
  };

  const btnAddHandler = () => {
    const { temp_input, temp_message, input_list } = state;
    const newInputList = [
      ...input_list,
      {
        id: `${new Date().getTime() / 1000}`,
        input: temp_input,
        message: temp_message,
      },
    ];
    setState({
      ...state,
      input_list: newInputList,
      temp_input: "",
      temp_message: "",
    });
  };

  const handleCheckShowTable = (event: any) => {
    const { checked } = event.target;
    setState({ ...state, showTable: checked });
  };

  const handleActionDelete = (id: string) => {
    const newInputList = state.input_list.filter((item) => item.id !== id);
    setState({ ...state, input_list: newInputList });
  };

  const handleEditButton = (item: ITodo) => {
    setState({ ...state, editItem: { id: item.id, message: item.message, input: item.input } });
  };

  const handleEditOnChange = (event: any) => {
    const { name, value } = event.target;
    if (name === "editInput") {
      setState({ ...state, editItem: { ...state.editItem, input: value } });
    } else if (name === "editMessage") {
      setState({ ...state, editItem: { ...state.editItem, message: value } });
    }
  };

  const handeUpdateButton = (event: any) => {
    const { editItem, input_list } = state;

    const newList = input_list.map((items) => {
      if (items.id === editItem.id) {
        return editItem;
      }
      return items;
    });
    setState({ ...state, input_list: newList, editItem: { id: "", input: "", message: "" } });
  };

  const handleCancelUpdate = () => {
    setState({ ...state, editItem: { id: "", input: "", message: "" } });
  };
   
  /*const handleButtonLogout =()=>{
    localStorage.removeItem('Token');
    navigate('/login');
  } */

  return (
<div>
    <AppBar position="static">
                  <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1, textAlign: 'left' }}>
                      Butchokoy App
                    </Typography>
                    <Button color="inherit">Login</Button>
                    <Button color="inherit">Users</Button>
                    <Button color="inherit">Post</Button>
                  </Toolbar>
      </AppBar>

    <div>
      <h1>Home Page</h1>
      <button>Logout</button>  
      <div className="basic-form-container">
        <TodoForm
          btnAddHandler={btnAddHandler}
          inputHandler={inputHandler}
          messageHandler={messageHandler}
          temp_input={state.temp_input}
          temp_message={state.temp_message}
        />
      </div>
      <input type="checkbox" onChange={handleCheckShowTable} /> <span id="span">Show Table</span>
      <TodoDisplay
        actionEdit={handleEditButton}
        editItem={state.editItem}
        actionDelete={handleActionDelete}
        showTable={state.showTable}
        inputlist={state.input_list}
        handleEditOnChange={handleEditOnChange}
        handeUpdateButton={handeUpdateButton}
        handleCancelUpdate={handleCancelUpdate}
      />
    
    </div>
    </div>
  );
};

export default Home;
