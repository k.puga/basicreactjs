import React, { useState } from "react";
import { IProp } from "./type";
import { useNavigate } from "react-router-dom";

import { accounts } from "../../utils/authenticateAccounts"; 

function Login(props: IProp) {
    const navigate = useNavigate();
    const [showPass, setShowPass] = useState<boolean>(false);
    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");


    const handleLogin = () =>{
        navigate('/');
      }
    


      const handleInputUser = (event: React.ChangeEvent<HTMLInputElement>) =>{
        const {value} = event.target;
        setUsername(value);
      }

      const handleInputPass = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target;
        setPassword(value);
      }

      const handleLoginButton = () => {
        const token = {
            username, password
        }
        const authenticated = accounts.map(item => JSON.stringify(item)).includes(JSON.stringify(token))
        if(authenticated){
            localStorage.setItem('Token', JSON.stringify(token))   
            navigate('/');
        }
        else{
            alert("Invalid Account");
        }
      }
    return(
        <div>
        <h1>Login</h1>
        <label> Username</label>
        <br/>
         <input type = "text"/>
         <br/>
         <label>Password</label>
         <br/>
         <input type = {showPass ? "text" : "password"} /> 
         <br/>
         <input type ="checkbox" checked={showPass} onChange={()=> setShowPass(prev => !prev)}/><span>Show Password</span>
         <br/>
         <button onClick={handleLogin}>Login</button>
         
        </div>
    
    )
      
    
}

export default Login